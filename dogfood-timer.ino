// include the library code:
#include <LiquidCrystal.h>
#include <Time.h>
// initialize the library by associating any needed LCD interface pin
// with the arduino pin number it is connected to
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
int a = 1, b = 5, c = 2, d = 5, e = 0, f = 4, g = 4, h = 1, i = 2, j = 7;
unsigned long timeset = 0;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
const int trigPin = 9;
const int echoPin = 8;
long duration;
int distance = 0;
unsigned long wait; 
time_t last = 0; 
unsigned long lastlong = 0;
int prevphoto = 0;
int today = 0;
int oldday;
boolean check = false;
void setup() {
  Serial.begin(9600);
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin, INPUT); // Sets the echoPin as an Input
  pinMode(10, OUTPUT); // pin that enables backlight
  digitalWrite(10, HIGH);
  setuptime();
}

void loop() {
  
}
void setuptime() {
  lcd.clear();
  lcd.setCursor(0, 1);
  lcd.print("Enter UNIX time");
  lcd.setCursor(0, 0);
  lcd.print(a);
  while (digitalRead(7) == 0) {
    if(digitalRead(6) == 1) {
      a+=1;
      if(a == 10) {
        a = 0;
      }
      lcd.setCursor(0, 0);
      lcd.print(a);
      delay(200);
    }
  }
  delay(250);
  lcd.setCursor(1, 0);
  lcd.print(b);
  while (digitalRead(7) == 0) {
    if(digitalRead(6) == 1) {
      b+=1;
      if(b == 10) {
        b = 0;
      }
      lcd.setCursor(1, 0);
      lcd.print(b);
      delay(200);
    }
  }
  delay(250);
  lcd.setCursor(2, 0);
  lcd.print(c);
  while (digitalRead(7) == 0) {
    if(digitalRead(6) == 1) {
      c+=1;
      if(c == 10) {
        c = 0;
      }
      lcd.setCursor(2, 0);
      lcd.print(c);
      delay(200);
    }
  }
  delay(250);
  lcd.setCursor(3, 0);
  lcd.print(d);
  while (digitalRead(7) == 0) {
    if(digitalRead(6) == 1) {
      d+=1;
      if(d == 10) {
        d = 0;
      }
      lcd.setCursor(3, 0);
      lcd.print(d);
      delay(200);
    }
  }
  delay(250);
  lcd.setCursor(4, 0);
  lcd.print(e);
  while (digitalRead(7) == 0) {
    if(digitalRead(6) == 1) {
      e+=1;
      if(e == 10) {
        e = 0;
      }
      lcd.setCursor(4, 0);
      lcd.print(e);
      delay(200);
    }
  }
  delay(250);
  lcd.setCursor(5, 0);
  lcd.print(f);
  while (digitalRead(7) == 0) {
    if(digitalRead(6) == 1) {
      f+=1;
      if(f == 10) {
        f = 0;
      }
      lcd.setCursor(5, 0);
      lcd.print(f);
      delay(200);
    }
  }
  delay(250);
  lcd.setCursor(6, 0);
  lcd.print(g);
  while (digitalRead(7) == 0) {
    if(digitalRead(6) == 1) {
      g+=1;
      if(g == 10) {
        g = 0;
      }
      lcd.setCursor(6, 0);
      lcd.print(g);
      delay(200);
    }
  }
  delay(250);
  lcd.setCursor(7, 0);
  lcd.print(h);
  while (digitalRead(7) == 0) {
    if(digitalRead(6) == 1) {
      h+=1;
      if(h == 10) {
        h = 0;
      }
      lcd.setCursor(7, 0);
      lcd.print(h);
      delay(200);
    }
  }
  delay(250);
  lcd.setCursor(8, 0);
  lcd.print(i);
  while (digitalRead(7) == 0) {
    if(digitalRead(6) == 1) {
      i+=1;
      if(i == 10) {
        i = 0;
      }
      lcd.setCursor(8, 0);
      lcd.print(i);
      delay(200);
    }
  }
  delay(250);
  lcd.setCursor(9, 0);
  lcd.print(j);
  while (digitalRead(7) == 0) {
    if(digitalRead(6) == 1) {
      j+=1;
      if(j == 10) {
        j = 0;
      }
      lcd.setCursor(9, 0);
      lcd.print(j);
      delay(200);
    }
  } // on the next line, for f I have to divide by 10 and add a zero otherwise it gives a negative number for some reason
  timeset = (1000000000 * a)+(100000000 * b)+(10000000 * c)+(1000000 * d)+(100000 * e)+(100000 * f / 10)+(1000 * g)+(100 * h)+(10 * i)+j;
  unsigned long timesetcst = timeset - 18000; //convert to central time from utc
  setTime(timesetcst);
  delay(250);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(timeset);
  lcd.setCursor(0, 1);
  lcd.print("is this ok?");
  while (digitalRead(7) == 0) {
    if(digitalRead(6) == 1) {
      delay(200);
      setuptime();
    }
  }
  delay(250);
  unsigned long oldtime = 0;
  unsigned long clockoffset = now();
  check = false;
  while(1 == 1) {
    time_t t = now();
    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    // Sets the trigPin on HIGH state for 10 micro seconds
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
    
    // Reads the echoPin, returns the sound wave travel time in microseconds
    duration = pulseIn(echoPin, HIGH);
    // Calculating the distance
    distance = duration*0.034/2;
    // Prints the distance on the Serial Monitor
    //Serial.print("Distance: ");
    //Serial.println(distance);
    

    if(distance < 30 || analogRead(3) >= 100 || digitalRead(7) == 1 || digitalRead(6) == 1) {
      digitalWrite(10, HIGH);
      wait = now();
    }
    if(digitalRead(7) == 1) {
      if(check == false){
        check = true;
        delay(100);
      } else {
        check = false;
        delay(100);
      }
    }
    if(digitalRead(6) == 1 && check == true) {
        delay(200);
        setuptime();
      }
    if(now() != oldtime) {
      //if(check == false) {
      //Serial.println(analogRead(3));
      if(now()-1500 >= clockoffset) {
        if(now()-15 >= wait && analogRead(3) < 100) {
          digitalWrite(10, LOW); // just in case the arduino skips a second at the wrong time
        }
        if(day(t) != oldday) {
          today = 0;
        }
        setTime(now() + 1);
        clockoffset = now();
      }
      if(now()-15 >= wait && analogRead(3) < 100) {
        digitalWrite(10, LOW);
      }
      if(day(t) != oldday) {
        today = 0;
      }
      lcd.setCursor(0,0);
      lcd.clear(); /*
      lcd.print(hourFormat12(t));
      lcd.print(":");
      if(minute(t) < 10) {
        lcd.print("0");
      }
      lcd.print(minute(t));
      lcd.print(" ");
      if(isAM(t) == true) {
        lcd.print("AM");
      }
      else {
        lcd.print("PM");
      } */

      if (analogRead(3) < 100 && prevphoto >=100) {
        last = now();
        lastlong = now();
        today ++;
      } 
      if(lastlong == 0) {
        lcd.print("LF: ");
        if (analogRead(3) > 100) {
          if(now() > 1526169600 && now() < 1526274000) {
              lcd.clear();
              lcd.print("Happy Mother's");
              lcd.setCursor(0, 1);
              lcd.print("Day! :)");
          } else {
          lcd.print("Can is open");
          }
        } else {
          lcd.print("never");
        }
      } 
      else {
        lcd.print("LF: ");
        if (analogRead(3) > 100) {
          if(now() > 1526169600 && now() < 1526274000) {
              lcd.clear();
              lcd.print("Happy Mother's");
              lcd.setCursor(0, 1);
              lcd.print("Day! :)");
          } else {
          lcd.print("Can is open");
          }
        } else {
          int hours = (int)(now() / 3600) - (int)(lastlong / 3600);
          int minutes = (now() / 60) - (lastlong / 60) - hours * 60;
          while(minutes < 0) {
            minutes = minutes + 60;
            hours --;
          }
          if(hours > 24) {
            lcd.print("over 24 hours ago");
          } else {
            lcd.print(hours);
            lcd.print(" hr "); 
            lcd.print(minutes);
            lcd.print(" min"); 
          }
        }
        if((now() < 1526169600 || now() > 1526274000) || analogRead(3) < 100) { 
        lcd.setCursor(0, 1);
        lcd.print(" Today: ");
        lcd.print(today);
        lcd.print(" Meal");
        if(today != 1) {
          lcd.print("s");
        }
        }
      }
      //lcd.setCursor(0, 1);
      //lcd.print(now() + 18000); //display unix time stamp on line 1
      oldday = day();
      oldtime = now();
      prevphoto = analogRead(3);
      if(check == true) {
        //check == true now
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(hourFormat12(t));
      lcd.print(":");
      if(minute(t) < 10) {
        lcd.print("0");
      }
      lcd.print(minute(t));
      lcd.print(" ");
      if(isAM(t) == true) {
        lcd.print("AM");
      }
      else {
        lcd.print("PM");
      }
      lcd.setCursor(0, 1);
      lcd.print(now());
      
      //delay(50);
    }    
      
      //} //else {
      
    }
    else {
      //int staticmillis = millis();
      //while(staticmillis + 50 > millis()) {
      delay(50);
      //}
    }
    
  }
}