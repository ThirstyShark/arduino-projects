int timeInterval = 0;
int analogPin = 5;
int drips = 0;
float grams = 30;
float totgrams = 0;
float phSense;
float mass = 39.99711;
bool titrate = false;
void setup() {
// put your setup code here, to run once:
  if (drips > 0) {
    grams = grams - (2.333333/20 * drips);
  }
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(9, INPUT); //stream; 2 seconds
  pinMode(10, OUTPUT);
  pinMode(11, INPUT); //squirt; 0.5 seconds
  pinMode(12, INPUT); //drip; 0.01 seconds
  Serial.begin(9600);
  digitalWrite(4, LOW); // start extended
}
void loop() {
  timeInterval=0;
  if(digitalRead(7) == 0) {
    digitalWrite(4, HIGH); // up and down
    delay(2500);
    digitalWrite(3, HIGH); // number 3 is spring-loaded
    delay(1500);
    digitalWrite(4, LOW);
    delay(250);
    motor(255);
    delay(3000);
    titrate = true; //true, false for now
    if(digitalRead(7) == 0 || titrate == false) {
        Serial.println("CANCELLING");
        stopmotor();
        titrate = false;
        digitalWrite(4, HIGH); // up and down
        delay(2500);
        digitalWrite(3, LOW); // number 3 is spring-loaded
        delay(1500);
        digitalWrite(4, LOW);
        delay(500);
        while(true) {
          loop();
        }
    }
  }
  if(digitalRead(9) == 0)
  {
    motor(255);
    delay(2000);
    stopmotor();
    //timeInterval = 175 * grams;
    //totgrams = totgrams + grams;
  }
  else if(digitalRead(11) == 0 || titrate == true)
  {
    takepH();
    float rate = 1 - (phSense / 7)*(phSense / 7);
    while (rate > 0 && rate <=1) {
      motor(255);
      if(digitalRead(7) == 0) {
          Serial.println("CANCELLING");
          titrate = false;
          stopmotor();
          digitalWrite(4, HIGH); // up and down
          delay(1500);
          digitalWrite(3, LOW); // number 3 is spring-loaded
          delay(2000);
          digitalWrite(4, LOW);
          while(true) {
            loop();
          }
      }
      
      if(rate > 0.8) {
        rate = rate * 2;
      }
      if(phSense > 3) {
          rate = 0.01;
      }
      timeInterval = rate * 175;
      if (timeInterval < 30) {
        timeInterval = 15;
      }
      totgrams = totgrams + timeInterval / 175;
      digitalWrite(2, HIGH);
      delay(timeInterval);
      digitalWrite(2, LOW);
      for(int i = 0; i < 10; i += 1){
        delay(rate * 900);
        if(digitalRead(7) == 0) {
            Serial.println("CANCELLING");
            titrate = false;
            stopmotor();
            digitalWrite(4, HIGH); // up and down
            delay(1500);
            digitalWrite(3, LOW); // spring-loaded
            delay(2000);
            digitalWrite(4, LOW);
            while(true) {
              loop();
            }
        }
      }
      takepH();
      rate = 1 - (phSense / 7)*(phSense / 7);
      delay(125);
    }
    Serial.println("done, returning pneumatics to holding point");
    titrate = false;
    stopmotor();
    digitalWrite(4, HIGH); // up and down
    delay(1500);
    digitalWrite(3, LOW); // spring-loaded
    delay(1500);
    digitalWrite(4, LOW);
    timeInterval = 0;
    //timeInterval = 500;
    //totgrams = totgrams + 2.857143;
  }
  else if (digitalRead(12) == 0 || drips > 0)
  {
    timeInterval = 15;
    totgrams = totgrams + 2.333333/20;
    drips --;
  }
  //Serial.print("Time Valve is open = ");
  //Serial.println(timeInterval);
  if(timeInterval > 0)
  {
    digitalWrite(2, HIGH);
    delay(timeInterval);
    digitalWrite(2, LOW);
    takepH();
  }
  delay(50);
}
float takepH()
{
  int samples = 30;
  int aRead = 0;
  for (int i = 0; i < samples ; i++)
  {
    aRead += analogRead(analogPin);
    delay(100);
  }
  phSense = 0;
  float phvolt = 5.0 * aRead/ (1023 * samples); // assuming 5V reference
  phSense = 14 - phvolt/0.25; // convert voltage to pH
  phSense = phSense - 1.3;
  Serial.print("Time Interval = ");
  Serial.println(timeInterval);
  Serial.print("Analog in reading: ");
  Serial.print(aRead/20); // print pH value on serial monitor
  Serial.print(" - Calculated pH Level: ");
  Serial.println(phSense, 2); // 1 = one decimal, 2 = two decimals, etc
  Serial.print("Grams: ");
  Serial.println(totgrams * 0.87, 4);
  float molarity = 0.0025 * totgrams * 0.9 / 49.6;
  Serial.print("Molarity of 20 mL of acid: ");
  Serial.println(molarity, 10);
  // removed the /10
  delay(500);
}
void motor(int speed) {
    analogWrite(10, speed);
    //delay(timerun);
    //analogWrite(10, 0);
}
void stopmotor() {
    analogWrite(10, 0);
    //delay(timerun);
    //analogWrite(10, 0);
}


